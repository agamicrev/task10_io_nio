package task2_serialization;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Task 2. Create Ship with Droids. Serialize and deserialize
 * them. Use transient.
 */
public class App {

  public static void main(String[] args) throws IOException, ClassNotFoundException {
    Ship ship = new Ship();
    ship.loadDroids();
    ship.serializeDroids();

    ArrayList<Droid> deserializedShip = ship.deserializeDroids();

    for (int i = 0; i < deserializedShip.size(); i++) {
      System.out.println("**** Deserialized ****" +
          "\nName: " + deserializedShip.get(i).getName() +
          "\nHealth: " + deserializedShip.get(i).getHealth() +
          "\nPower: " + deserializedShip.get(i).getPower() +
          "\nAlive (transient): " + deserializedShip.get(i).getIsAlive());
    }

  }
}
