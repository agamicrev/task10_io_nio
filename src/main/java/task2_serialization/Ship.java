package task2_serialization;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Ship {

  public static List<Droid> droidsShip = new ArrayList<>();

  public static void loadDroids() {
    droidsShip.add(new Droid("R2D2", 900, 90, false));
    droidsShip.add(new Droid("R3D3", 750, 80, true));
    droidsShip.add(new Droid("R4D4", 700, 90, true));
  }

  public static void serializeDroids() throws IOException {
    FileOutputStream fos = new FileOutputStream("droids.dat");
    ObjectOutputStream oos = new ObjectOutputStream(fos);
    oos.writeObject(droidsShip);
    fos.close();
    oos.close();
  }

  public static ArrayList<Droid> deserializeDroids() throws IOException, ClassNotFoundException {
    FileInputStream fis = new FileInputStream("droids.dat");
    ObjectInputStream ois = new ObjectInputStream(fis);
    ArrayList<Droid> tempList = (ArrayList) ois.readObject();
    fis.close();
    ois.close();
    return tempList;
  }

}
