package task3_compare_reading;

import java.io.*;
import java.util.Date;
import task4_my_input_stream.MyInputStream;

/**
 * Task 3. Compare reading and writing performance of usual and
 * buffered reader for 2 MB file.
 * Compare performance of buffered reader with different buffer
 * size (e.g. 10 different size).
 */
public class App {

  private static FileInputStream fis;

  public static void main(String[] args) throws IOException {

    // file variable is created for calling on its length() method
    File file = new File("text.txt");
    System.out.println("File size: " + file.length() / 1024 + " Kb");

    long bufferedTime = timeBufferedReader();
    System.out.println("Buffered Reader [size 8 MB] takes: " + bufferedTime + " ms");

    bufferedTime = timeBufferedReader2();
    System.out.println("Buffered Reader [size 12 MB] takes: " + bufferedTime + " ms");

    bufferedTime = timeBufferedReader3();
    System.out.println("Buffered Reader [size 16 MB] takes: " + bufferedTime + " ms");

    long usualTime = timeUsualReader();
    System.out.println("Usual reading takes: " + usualTime + " ms");

    System.out.println(bufferedTime < usualTime ?
        "*** Buffered Reader is faster! ***" : "*** Usual reading is faster! ***");

    long myTime = MyReader();
    System.out.println("Reader with MyInputStream takes: " + myTime + " ms");
  }

  private static long timeUsualReader() throws IOException {
    fis = new FileInputStream("text.txt");
    // time start point
    Date dateStart = new Date();
    while (fis.available() > 0) {
      fis.read();
    }
    // time end point
    Date dateEnd = new Date();
    fis.close();

    // return the execution time
    return dateEnd.getTime() - dateStart.getTime();
  }

  private static long timeBufferedReader() throws IOException {
    fis = new FileInputStream("text.txt");
    BufferedInputStream bis = new BufferedInputStream(fis, 8 * 1024);
    Date dateStart = new Date();
    while (bis.available() > 0) {
      bis.read(new byte[1024]);
    }
    Date dateEnd = new Date();
    fis.close();
    bis.close();

    // return the execution time
    return dateEnd.getTime() - dateStart.getTime();
  }

  // BufferedReader2 has bigger size
  private static long timeBufferedReader2() throws IOException {
    fis = new FileInputStream("text.txt");
    BufferedInputStream bis = new BufferedInputStream(fis, 12 * 1024);
    Date dateStart = new Date();
    while (bis.available() > 0) {
      bis.read(new byte[1024]);
    }
    Date dateEnd = new Date();
    fis.close();
    bis.close();

    // return the execution time
    return dateEnd.getTime() - dateStart.getTime();
  }

  // BufferedReader3 has the biggest size
  private static long timeBufferedReader3() throws IOException {
    fis = new FileInputStream("text.txt");
    BufferedInputStream bis = new BufferedInputStream(fis, 16 * 1024);
    Date dateStart = new Date();
    while (bis.available() > 0) {
      bis.read(new byte[1024]);
    }
    Date dateEnd = new Date();
    fis.close();
    bis.close();

    // return the execution time
    return dateEnd.getTime() - dateStart.getTime();
  }

  // Lets test MyInputStream from task4
  private static long MyReader() throws IOException {
    fis = new FileInputStream("text.txt");
    MyInputStream myis = new MyInputStream(fis, 16 * 1024);
    Date dateStart = new Date();
    while (myis.available() > 0) {
      myis.read(new byte[1024]);
    }
    Date dateEnd = new Date();
    fis.close();
    myis.close();

    // return the execution time
    return dateEnd.getTime() - dateStart.getTime();
  }
}
