package task5_reading_comments;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Task 5. Write a program that reads a Java source-code file (you
 * provide the file name on the command line) and displays all the
 * comments. Do not use regular expression.
 */
public class App {

  private static List<String> list = new ArrayList<>();

  public static void main(String[] args) throws IOException {
    readComments("D:\\task10_IO_NIO\\src\\main\\java\\task3_compare_reading\\App.java");
    System.out.println("**** Сomments **** ");
    list.forEach(System.out::println);
  }

  private static void readComments(String fileName) throws IOException {
    BufferedReader reader = new BufferedReader(new FileReader(fileName));
    while (reader.ready()) {
      String line = reader.readLine().trim();
      if (line.startsWith("//")) {
        list.add(line);
      }
    }
  }
}
