package task6_display_directory;

import java.io.IOException;

@FunctionalInterface
public interface Printable {
  void print() throws IOException;
}
