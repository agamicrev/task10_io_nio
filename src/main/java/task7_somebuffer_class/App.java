package task7_somebuffer_class;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

/**
 * Task 7. Try to create SomeBuffer class, which can be used for read
 * and write data from/to channel (Java NIO)
 */
public class App {

  private static Path readPath() throws FileNotFoundException {
    return Paths.get("somebuffer.txt");
  }

  private static void readWithFileChannel(Path path) throws IOException {
    FileChannel fileChannel = FileChannel.open(readPath());
    ByteBuffer byteBuffer = ByteBuffer.allocate(1024);
    int read = 0;
    while ((read = fileChannel.read(byteBuffer)) != -1) {
      byteBuffer.flip();
      while (byteBuffer.hasRemaining()) {
        System.out.print((char) byteBuffer.get());
      }
      byteBuffer.clear();
    }
    fileChannel.close();
  }

  private static void writeWithFileChannel(Path path) throws IOException {
    FileChannel fileChannel = FileChannel.open(path, StandardOpenOption.WRITE);
    String line = "We can write data to channel";
    ByteBuffer byteBuffer = ByteBuffer.wrap(line.getBytes());
    byteBuffer.rewind();
    fileChannel.write(byteBuffer);
    fileChannel.close();

  }

  public static void main(String[] args) throws IOException {
    readWithFileChannel(readPath());
    writeWithFileChannel(readPath());
  }

}
