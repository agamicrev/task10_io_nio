package task8_client_server;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

/**
 * Task 8. Write client-server program using NIO (+ if you want, other
 * implementation using IO). E.g. you have one server and multiple clients.
 * A client can send direct messages to other client.
 */
public class Client {

  private static final int BUFFER_SIZE = 1024;
  private static String[] messages =
      {"Hello! My name is Andrew.",
          "I am big fan of Chelsea football club.",
          "Blue is the colour, football is the game.",
          "We are all together and winning is our aim.",
          "*exit*"};

  public static void main(String[] args) {

    logger("Starting Client...");
    try {
      int port = 9999;
      InetAddress hostIP = InetAddress.getLocalHost();
      InetSocketAddress myAddress =
          new InetSocketAddress(hostIP, port);
      SocketChannel myClient = SocketChannel.open(myAddress);

      logger(String.format("Trying to connect to %s:%d...",
          myAddress.getHostName(), myAddress.getPort()));

      for (String msg : messages) {
        ByteBuffer myBuffer = ByteBuffer.allocate(BUFFER_SIZE);
        myBuffer.put(msg.getBytes());
        myBuffer.flip();
        int bytesWritten = myClient.write(myBuffer);
        logger(String
            .format("Sending Message...: %s\nbytesWritten...: %d",
                msg, bytesWritten));
      }
      logger("Closing Client connection...");
      myClient.close();
    } catch (IOException e) {
      logger(e.getMessage());
      e.printStackTrace();
    }
  }

  public static void logger(String msg) {
    System.out.println(msg);
  }
}
